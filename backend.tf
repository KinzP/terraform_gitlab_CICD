# --- root/backend.tf ---

terraform {
  backend "s3" {
    bucket = "kp005gitlabcicd"
    key    = "remote.tfstate"
    region = "us-east-1"
  }
}